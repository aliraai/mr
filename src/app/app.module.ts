import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SignupComponent } from './authentication/signup/signup.component';
import { ContactComponent } from './contact/contact.component';
import { SigninComponent } from './authentication/signin/signin.component';
import { AboutComponent } from './about/about.component';
import { IndustriesComponent } from './industries/industries.component';
import { HomeComponent } from './home/home.component';
import { RegionsComponent } from './regions/regions.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExpertiseComponent } from './expertise/expertise.component';
import { InsightsComponent } from './insights/insights.component';
import { NavbarComponent } from './common/header/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AboutUSComponent } from './aboutus/who-are-u/about-us.component';

import { from } from 'rxjs';
import { MaterealModule } from './matereal/matereal.module';
import { BlogComponent } from './Media/blog/blog.component';
import { PressReleasesComponent } from './Media/press-releases/press-releases.component';
import { SearchComponent } from './common/header/navbar/search/search.component';




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,

    AboutUSComponent,
    BlogComponent,
    PressReleasesComponent,
    ExpertiseComponent,
    InsightsComponent,
    NavbarComponent,
    BlogComponent,
    PressReleasesComponent,
    HomeComponent,
    AboutComponent,
    SigninComponent,
    SignupComponent,
    ContactComponent,
    IndustriesComponent,
    RegionsComponent,



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterealModule,
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
