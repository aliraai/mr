import { NgModule } from '@angular/core';
import { MatSelectModule, MatButtonModule, MatToolbarModule, MatButtonToggle, MatButtonToggleModule, MatProgressSpinner, MatProgressSpinnerModule, MatCard, MatCardModule } from '@angular/material';
import { from } from 'rxjs';




const material = [
  MatButtonModule,
  MatToolbarModule,
  MatButtonToggleModule,
  MatProgressSpinnerModule,
  MatCardModule
];

@NgModule({
  declarations: [],
  imports: [material],
  exports: [material]
})
export class MaterealModule { }
