import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngamentStrategiesComponent } from './engament-strategies.component';

describe('EngamentStrategiesComponent', () => {
  let component: EngamentStrategiesComponent;
  let fixture: ComponentFixture<EngamentStrategiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngamentStrategiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngamentStrategiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
