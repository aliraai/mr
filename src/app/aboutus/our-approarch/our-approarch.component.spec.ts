import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OurApproarchComponent } from './our-approarch.component';

describe('OurApproarchComponent', () => {
  let component: OurApproarchComponent;
  let fixture: ComponentFixture<OurApproarchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurApproarchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OurApproarchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
