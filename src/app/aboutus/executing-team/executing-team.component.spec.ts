import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutingTeamComponent } from './executing-team.component';

describe('ExecutingTeamComponent', () => {
  let component: ExecutingTeamComponent;
  let fixture: ComponentFixture<ExecutingTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutingTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutingTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
