import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OurJourneryComponent } from './our-journery.component';

describe('OurJourneryComponent', () => {
  let component: OurJourneryComponent;
  let fixture: ComponentFixture<OurJourneryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurJourneryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OurJourneryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
