import { RegionsComponent } from './regions/regions.component';
import { PressReleasesComponent } from './Media/press-releases/press-releases.component';
import { BlogComponent } from './Media/blog/blog.component';
import { IndustriesComponent } from './industries/industries.component';
import { SignupComponent } from './authentication/signup/signup.component';
import { SigninComponent } from './authentication/signin/signin.component';
import { ContactComponent } from './contact/contact.component';
import { InsightsComponent } from './insights/insights.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: '', component: HomeComponent },
  {path: 'about', component: AboutComponent },
  {path: 'insights', component: InsightsComponent },
  {path: 'industries', component: IndustriesComponent },
  {path: 'blog', component: BlogComponent },
  {path: 'press-releases', component: PressReleasesComponent },
  {path: 'regions', component: RegionsComponent },
  {path: 'contact', component: ContactComponent},
  {path: 'signin', component: SigninComponent },
  {path: 'signup', component: SignupComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
